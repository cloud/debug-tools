FROM debian:buster-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
	apt-get install -y --no-install-recommends \
	# General
	bash-completion \
	ca-certificates \
	vim-tiny \
	nano \
	htop \
	jq \
	screen \
	tmux \
	tree \
	less \
	# Network
	net-tools \
	dnsutils \
	tcpdump \
	traceroute \
	iputils* \
	curl \
	wget \
	telnet \
	iperf \
	qperf \
	bridge-utils \
	# IO
	iotop \
	sysstat \
	fio \
	# System
	lshw \
	kmod \
	smem \
	smemcap \
	procps \
	strace \
	pciutils \
	dmidecode \
	ipmitool \
	systemd && \
	apt-get clean && \
	rm -r /var/lib/apt/lists/* && \
	update-alternatives --install /usr/bin/vim vim /usr/bin/vim.tiny 15
